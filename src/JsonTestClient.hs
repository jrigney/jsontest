{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module JsonTestClient
  ( ipAddressRequest
  , addr
  ) where

import Control.Lens
import Data.Aeson
import Data.Proxy
import Debug.Trace (trace)
import GHC.Generics
import Network.HTTP.Client (Manager)
import Servant.API
import Servant.Client
       (BaseUrl(BaseUrl), ClientEnv(ClientEnv), ClientM, Scheme(Http),
        ServantError, client, runClientM)

-- create a lens for IpAddress to avoid exposing the IpAddress constructor
-- and in this instance we do not even need to expose IpAddress either
addr :: Lens' IpAddress String
addr = lens ip (\ipaddr v -> ipaddr {ip = v})

-- define as a type what the endpoint should return.
type API = Get '[ JSON] IpAddress

-- need to figure out this
api :: Proxy API
api = Proxy

-- client returns client functions for the entire AP as defined by API.
ipAddressApi :: ClientM IpAddress
ipAddressApi = client api

-- set the base url to 'http://ip.jsontest.com:80/'
-- the empty string is the path
jsonTestHost :: BaseUrl
jsonTestHost = BaseUrl Http "ip.jsontest.com" 80 ""

-- this is exported and is what makes the request
ipAddressRequest :: Manager -> IO (Either ServantError IpAddress)
ipAddressRequest mgr = runClientM ipAddress (ClientEnv mgr jsonTestHost)

-- the monad in which the client functions run
ipAddress :: ClientM (IpAddress)
ipAddress = do
  ip <- ipAddressApi
  return (ip)

data IpAddress = IpAddress
  { ip :: String
  } deriving (Show, Generic)

instance FromJSON IpAddress
