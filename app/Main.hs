module Main where

import Lib
import Control.Lens
import JsonTestClient(ipAddressRequest, addr)
import Network.HTTP.Client(newManager,defaultManagerSettings)
import Control.Monad.IO.Class (liftIO)

main :: IO ()
main = do
    manager <- newManager defaultManagerSettings
    resp <- liftIO (ipAddressRequest manager )
    case resp of
      Left anError -> putStrLn "error"
      Right val -> putStrLn (view addr val)
