# jsontest
A basic servant client that hits ip.jsontest.com and gets your IP address in
JSON form.

## Build
stack build

## Run
stack exec jsontest-exe
